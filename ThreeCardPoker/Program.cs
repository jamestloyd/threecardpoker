﻿using System;
using System.Collections.Generic;
using System.Linq;
using ThreeCardPoker.Models;

namespace ThreeCardPoker
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                var playerCountInput = DoReadStdIn();

                var playerCountParsed = int.Parse(playerCountInput);
                if (playerCountParsed > 24)
                {
                    throw new ApplicationException("Players cannot exceed 24.");
                }

                var players = new List<Player>();
                for (int i = 0; i < playerCountParsed; i++)
                {
                    var playerInput = DoReadStdIn();
                    players.Add(GetPlayer(playerInput, i));
                }

                var table = new Table(players);

                DoPrintStdOut(string.Join(" ", table.GetWinningPlayers()));
                DoWaitForContinue();
            }
            catch (Exception ex)
            {
                DoPrintStdOut($"There has been an error. The error message is: \"{ex.Message}\"\nPlease rerun the application and try again.");
                DoWaitForContinue();
            }
        }

        private static Player GetPlayer(string playerInput, int expectedId)
        {
            if (string.IsNullOrWhiteSpace(playerInput))
            {
                throw new ApplicationException("No data was entered.");
            }

            var splitPlayerInput = playerInput.Split(" ");
            //we know by the document, that the first position is the integer id of the player
            var parsedPlayerId = int.Parse(splitPlayerInput[0]);
            if(parsedPlayerId != expectedId) throw  new ApplicationException($"Application expected {expectedId} for the player id, however it received {parsedPlayerId}.");
            var cards = splitPlayerInput.Skip(1);
            return new Player
            {
                Id = parsedPlayerId,
                Hand = new Hand
                {
                    Cards = cards.Select(x => new Card(x[0].ToString(), x[1].ToString()))
                }
            };
        }

        private static void DoPrintStdOut(string message)
        {
            Console.WriteLine(message);
        }

        private static string DoReadStdIn()
        {
            return Console.ReadLine();
        }

        private static void DoWaitForContinue()
        {
            DoPrintStdOut("Please press any key to continue...");
            Console.ReadKey();
        }
    }
}
