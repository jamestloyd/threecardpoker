namespace ThreeCardPoker.Enums
{
    public enum RankType
    {
        //this order matches how the ranks were laid out in the instructions
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        Ten,
        Jack,
        Queen,
        King,
        Ace
    }
}