namespace ThreeCardPoker.Enums
{
    public enum SuitType
    {
        //this order matches how the suits were laid out in instructions
        Hearts,
        Diamonds,
        Spades,
        Clubs,

    }
}