namespace ThreeCardPoker.Enums
{
    public enum HandType
    {
        //This enum is ordered from best hand to worst
        StraightFlush,
        ThreeOfAKind,
        Straight,
        Flush,
        Pair,
        HighCard
    }
}