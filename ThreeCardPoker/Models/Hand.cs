using System;
using System.Collections.Generic;
using System.Linq;
using ThreeCardPoker.Enums;

namespace ThreeCardPoker.Models
{
    public class Hand
    {
        public IEnumerable<Card> Cards { get; set; }

        public bool HasThreeCards => Cards.Count() == 3;
        public IEnumerable<Card> DuplicateCards => Cards.GroupBy(x => new { x.Rank, x.Suit }).Where(x => x.Count() > 1).SelectMany(x => x).Skip(1); //skip the first instance since technically its not a duplicate

        public RankType HighRank => Cards.OrderBy(x => x.Rank).First().Rank;

        public HandType HandType
        {
            get
            {
                //we need to check for two common cases first
                var isStraight = IsStraight();
                var isFlush = IsFlush();

                if (isStraight && isFlush)
                {
                    return HandType.StraightFlush;
                }

                if (isStraight)
                {
                    return HandType.Straight;
                }

                if (isFlush)
                {
                    return HandType.Flush;
                }

                if (IsThreeOfAKind())
                {
                    return HandType.ThreeOfAKind;
                }

                if (IsPair())
                {
                    return HandType.Pair;
                }

                return HandType.HighCard;
            }
        }

        private bool IsThreeOfAKind()
        {
            return Cards.GroupBy(x => x.Rank).Count() == 1;
        }

        private bool IsStraight()
        {
            var cardValues = Cards.Select(x => x.Value).OrderBy(x => x).ToList();

            if (Cards.Any(x => x.IsAce))
            {
                cardValues.Add(14);
            }

            var consecutive = 1;
            var maxConsecutive = 1;
            var previous = -1;
            foreach (var cardValue in cardValues)
            {
                if (cardValue == previous + 1)
                {
                    consecutive += 1;
                }
                else
                {
                    maxConsecutive = consecutive;
                    consecutive = 1;
                }

                previous = cardValue;
            }


            return Math.Max(maxConsecutive, consecutive) == 3;
        }

        private bool IsFlush()
        {
            return Cards.GroupBy(x => x.Suit).Count() == 1;
        }

        private bool IsPair()
        {
            return Cards.GroupBy(x => x.Rank).Count() == 2;
        }
    }
}