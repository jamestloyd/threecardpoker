using System;
using ThreeCardPoker.Enums;

namespace ThreeCardPoker.Models
{
    public class Card
    {

        public Card(string rank, string suit)
        {
            Rank = GetRankType(rank);
            Suit = GetSuitType(suit);
            RankDisplay = rank;
            SuitDisplay = suit;
        }

        public RankType Rank { get; }
        public SuitType Suit { get; }

        public string RankDisplay { get; }
        public string SuitDisplay { get; }

        public bool IsAce => Rank == RankType.Ace;

        public int Value
        {
            get
            {
                switch (Rank)
                {
                    case RankType.Ace:
                        return 1;
                    case RankType.Two:
                        return 2;
                    case RankType.Three:
                        return 3;
                    case RankType.Four:
                        return 4;
                    case RankType.Five:
                        return 5;
                    case RankType.Six:
                        return 6;
                    case RankType.Seven:
                        return 7;
                    case RankType.Eight:
                        return 8;
                    case RankType.Nine:
                        return 9;
                    case RankType.Ten:
                        return 10;
                    case RankType.Jack:
                        return 11;
                    case RankType.Queen:
                        return 12;
                    default: //assume the king
                        return 13;
                }
            }
        }
        private SuitType GetSuitType(string suit)
        {
            switch (suit.ToLower())
            {
                case "h":
                    return SuitType.Hearts;
                case "d":
                    return SuitType.Diamonds;
                case "s":
                    return SuitType.Spades;
                case "c":
                    return SuitType.Clubs;
                default:
                    throw new ApplicationException("Unknown suit of cards encountered.");
            }
        }

        private RankType GetRankType(string rank)
        {
            switch (rank.ToLower())
            {
                case "2":
                    return RankType.Two;
                case "3":
                    return RankType.Three;
                case "4":
                    return RankType.Four;
                case "5":
                    return RankType.Five;
                case "6":
                    return RankType.Six;
                case "7":
                    return RankType.Seven;
                case "8":
                    return RankType.Eight;
                case "9":
                    return RankType.Nine;
                case "t":
                    return RankType.Ten;
                case "j":
                    return RankType.Jack;
                case "q":
                    return RankType.Queen;
                case "k":
                    return RankType.King;
                case "a":
                    return RankType.Ace;
                default:
                    throw new ApplicationException("Unkown card rank encountered.");
            }
        }

        public override string ToString()
        {
            return $"{RankDisplay}{SuitDisplay}";
        }
    }
}