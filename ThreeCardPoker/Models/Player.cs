using System.Collections.Generic;
using System.Linq;
using ThreeCardPoker.Enums;

namespace ThreeCardPoker.Models
{
    public class Player
    {
        public int Id {get;set;}

        public Hand Hand {get;set;}

        public HandType HandType => Hand.HandType;

        public RankType HighestCardRank => Hand.HighRank;

        public bool HasThreeCards => Hand.HasThreeCards;
        
        public IEnumerable<Card> DuplicateCards => Hand.DuplicateCards;
        
        public bool HasDuplicateCards => DuplicateCards.Any();

        public override string ToString()
        {
            return $"{Id}";
        }
    }
}