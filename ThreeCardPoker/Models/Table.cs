
using System;
using System.Collections.Generic;
using System.Linq;
using ThreeCardPoker.Enums;
// ReSharper disable PossibleMultipleEnumeration

namespace ThreeCardPoker.Models
{
    public class Table
    {
        public Table(IEnumerable<Player> players)
        {
            var playersWithout3Cards = players.Where(x => !x.HasThreeCards);
            if (playersWithout3Cards.Any())
            {
                throw new ApplicationException($"The following players do not have 3 cards or may have more than 3: {string.Join(" ",playersWithout3Cards)}");
            }

            var duplicatedCards = GetDuplicateCardsEncountered(players);
            if(duplicatedCards.Any())
            {
                var playersWithDuplicates = duplicatedCards.Select(x => $"Player {x.Item1.Id} has the following duplicates: {string.Join(" ", x.Item2)}");
                throw new ApplicationException($"The following duplicated cards were detected: {string.Join("\n", playersWithDuplicates)}");
            }

            Players = players;
        }

        public IEnumerable<Player> Players { get;}

        private IEnumerable<Tuple<Player, IEnumerable<Card>>> GetDuplicateCardsEncountered(IEnumerable<Player> players)
        {
            return (from player in players 
                    where player.HasDuplicateCards 
                    select new Tuple<Player, IEnumerable<Card>>(player, player.DuplicateCards));
        }

        public IEnumerable<Player> GetWinningPlayers()
        {

            var playersWinningWithStraightFlush = GetWinningPlayers(x => x.HandType == HandType.StraightFlush);
            if (playersWinningWithStraightFlush.Any())
            {
                return playersWinningWithStraightFlush;
            }

            var playersWinningWithThreeOFAKind = GetWinningPlayers(x => x.HandType == HandType.ThreeOfAKind);
            if (playersWinningWithThreeOFAKind.Any())
            {
                return playersWinningWithThreeOFAKind;
            }

            var playersWinningWithStraight = GetWinningPlayers(x => x.HandType == HandType.Straight);
            if (playersWinningWithStraight.Any())
            {
                return playersWinningWithStraight;
            }

            var playersWInningWithFlush = GetWinningPlayers(x => x.HandType == HandType.Flush);
            if (playersWInningWithFlush.Any())
            {
                return playersWInningWithFlush;
            }

            var playersWinningWithPair = GetWinningPlayers(x => x.HandType == HandType.Pair);
            if(playersWinningWithPair.Any()){
                return playersWinningWithPair;
            }

            var playersWinningWithHighCard = GetWinningPlayers(x => x.HandType == HandType.HighCard);
            return playersWinningWithHighCard;
        }

        private IEnumerable<Player> GetWinningPlayers(Func<Player, bool> filter)
        {
            if (Players.Count(filter) == 1)
            {
                return new[]
                {
                    Players.First(filter)
                };
            }

            var groupedCards = Players.Where(filter).GroupBy(x => x.HandType)
                                      .OrderByDescending(x => x.Key)
                                      .SelectMany(g => g.GroupBy(c => c.HighestCardRank)
                                      .OrderByDescending(x => x.Key).First());
            
            return groupedCards;
        }
    }
}